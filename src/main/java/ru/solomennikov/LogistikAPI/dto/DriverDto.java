package ru.solomennikov.LogistikAPI.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.solomennikov.LogistikAPI.persist.entity.VehicleType;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DriverDto {

    @NotEmpty
    @Size(min = 2, max = 50)
    private String name;

    private TreeSet<VehicleType> license = new TreeSet<>();

    @JsonProperty("valid_until")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate validUntil;

    private List<VehicleDto> vehicles = new ArrayList<>();
}
