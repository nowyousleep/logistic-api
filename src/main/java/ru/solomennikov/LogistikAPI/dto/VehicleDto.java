package ru.solomennikov.LogistikAPI.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.solomennikov.LogistikAPI.persist.entity.VehicleType;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VehicleDto {

    @NotEmpty
    @Size(min = 2, max = 20)
    private String name;

    @NotEmpty
    @JsonProperty("vehicle_type")
    private VehicleType vehicleType;

    @NotEmpty
    @JsonProperty("driver_id")
    private int driverId;

}
