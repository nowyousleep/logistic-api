package ru.solomennikov.LogistikAPI.persist.entity;

import lombok.Getter;


@Getter
public enum VehicleType {
    B, C, D
}
