package ru.solomennikov.LogistikAPI.persist.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "driver")
public class DriverEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "license")
    private Set<VehicleType> license = new TreeSet<>();

    @Column(name = "valid_until")
    private LocalDate validUntil;

    @OneToMany(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "driver_id")
    private List<VehicleEntity> vehicles = new ArrayList<>();
}
