package ru.solomennikov.LogistikAPI.persist.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.solomennikov.LogistikAPI.persist.entity.DriverEntity;

import java.util.List;

@Repository
public interface DriverRepository extends JpaRepository<DriverEntity, Integer> {
    @Query("select d from DriverEntity d join fetch d.vehicles")
    List<DriverEntity> findAllDrivers();
}
