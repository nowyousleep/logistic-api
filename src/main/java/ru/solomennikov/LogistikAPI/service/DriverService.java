package ru.solomennikov.LogistikAPI.service;

import ru.solomennikov.LogistikAPI.dto.DriverDto;

import java.util.List;

public interface DriverService {
    DriverDto addNewDriver(DriverDto dto);

    DriverDto showDriverById(int id);

    DriverDto updateDriver(int id, DriverDto dto);

    List<DriverDto> findAll();

    void destroy(int id);
}
