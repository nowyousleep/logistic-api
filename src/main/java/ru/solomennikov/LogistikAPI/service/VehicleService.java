package ru.solomennikov.LogistikAPI.service;

import ru.solomennikov.LogistikAPI.dto.VehicleDto;

import java.util.List;

public interface VehicleService {
    VehicleDto addNewVehicle(VehicleDto dto);

    List<VehicleDto> getAllVehicles();

    VehicleDto updateVehicle(int id, VehicleDto dto);

    VehicleDto getVehicle(int id);

    void destroyVehicle(int id);
}
