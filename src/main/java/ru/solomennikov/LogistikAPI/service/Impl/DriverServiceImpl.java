package ru.solomennikov.LogistikAPI.service.Impl;

import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;
import ru.solomennikov.LogistikAPI.dto.DriverDto;
import ru.solomennikov.LogistikAPI.dto.VehicleDto;
import ru.solomennikov.LogistikAPI.exception.NotFoundException;
import ru.solomennikov.LogistikAPI.mapper.DriverMapper;
import ru.solomennikov.LogistikAPI.mapper.VehicleMapper;
import ru.solomennikov.LogistikAPI.persist.entity.DriverEntity;
import ru.solomennikov.LogistikAPI.persist.entity.VehicleEntity;
import ru.solomennikov.LogistikAPI.persist.repository.DriverRepository;
import ru.solomennikov.LogistikAPI.service.DriverService;

import java.util.ArrayList;
import java.util.List;

import static org.apache.logging.log4j.util.Strings.isBlank;

@Service
@RequiredArgsConstructor
public class DriverServiceImpl implements DriverService {

    private final DriverRepository repository;
    private final DriverMapper driverMapper = Mappers.getMapper(DriverMapper.class);
    private final VehicleMapper vehicleMapper = Mappers.getMapper(VehicleMapper.class);
    @Override
    public DriverDto addNewDriver(DriverDto dto) {
        DriverEntity driverEntity = driverMapper.mapToEntity(dto);
        DriverEntity savedEntity = repository.save(driverEntity);
        return driverMapper.mapToDto(savedEntity);
    }

    @Override
    public DriverDto showDriverById(int id) {
        DriverEntity driverEntity = repository.findById(id).orElseThrow(() ->
                new NotFoundException("Driver with ID = " + id + " not found"));
        DriverDto driver = driverMapper.mapToDto(driverEntity);
        for(VehicleDto vehicle : driver.getVehicles()) {
            vehicle.setDriverId(id);
        }
        return driver;
    }

    @Override
    public DriverDto updateDriver(int id, DriverDto dto) {
        DriverEntity driverEntity = repository.findById(id).orElseThrow(() ->
                new NotFoundException("Driver with ID = " + id + " not found"));
        if(!isBlank(dto.getName())) {driverEntity.setName(dto.getName());}
        if(!dto.getLicense().isEmpty()) {driverEntity.setLicense(dto.getLicense());}
        if(!dto.getVehicles().isEmpty()) {
            List<VehicleEntity> vehicleEntities = new ArrayList<>(dto.getVehicles().size());
            for (VehicleDto vehicle : dto.getVehicles()) {
                vehicleEntities.add(vehicleMapper.mapToEntity(vehicle));
            }
            driverEntity.setVehicles(vehicleEntities);
        }
        if (dto.getValidUntil() != null) {driverEntity.setValidUntil(dto.getValidUntil());}
        DriverEntity updatedEntity = repository.save(driverEntity);
        return driverMapper.mapToDto(updatedEntity);
    }

    @Override
    public List<DriverDto> findAll() {
        List<DriverEntity> driverEntities = repository.findAllDrivers();
        List<DriverDto> driverDtos = new ArrayList<>(driverEntities.size());
        for(DriverEntity driver : driverEntities) {
            DriverDto resultDto = driverMapper.mapToDto(driver);
            for(VehicleDto vehicle : resultDto.getVehicles()) {
                vehicle.setDriverId(driver.getId());
            }
            driverDtos.add(resultDto);
        }
        return driverDtos;
    }

    @Override
    public void destroy(int id) {
        DriverEntity driverEntity = repository.findById(id).orElseThrow(() ->
                new NotFoundException("Driver with ID = " + id + " not found"));
        repository.delete(driverEntity);
    }
}
