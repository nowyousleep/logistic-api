package ru.solomennikov.LogistikAPI.service.Impl;

import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;
import ru.solomennikov.LogistikAPI.dto.VehicleDto;
import ru.solomennikov.LogistikAPI.exception.BadRequestException;
import ru.solomennikov.LogistikAPI.exception.NotFoundException;
import ru.solomennikov.LogistikAPI.mapper.VehicleMapper;
import ru.solomennikov.LogistikAPI.persist.entity.DriverEntity;
import ru.solomennikov.LogistikAPI.persist.entity.VehicleEntity;
import ru.solomennikov.LogistikAPI.persist.repository.DriverRepository;
import ru.solomennikov.LogistikAPI.persist.repository.VehicleRepository;
import ru.solomennikov.LogistikAPI.service.VehicleService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.hibernate.internal.util.StringHelper.isBlank;

@Service
@RequiredArgsConstructor
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository vehicleRepository;
    private final DriverRepository driverRepository;
    private final VehicleMapper mapper = Mappers.getMapper(VehicleMapper.class);

    @Override
    public VehicleDto addNewVehicle(VehicleDto dto) {
        DriverEntity driver = driverRepository.findById(dto.getDriverId()).orElseThrow(() ->
                new NotFoundException("Driver with ID = " + dto.getDriverId() + " not found"));
        if (checkDriver(driver, dto)) {
            VehicleEntity vehicleEntity = mapper.mapToEntity(dto);
            vehicleEntity.setDriver(driver);
            VehicleEntity savedEntity = vehicleRepository.save(vehicleEntity);
            VehicleDto returnDto = mapper.mapToDto(savedEntity);
            returnDto.setDriverId(dto.getDriverId());
            return returnDto;
        } else throw new BadRequestException("You can't add this vehicle to driver with ID = " + dto.getDriverId());
    }

    @Override
    public List<VehicleDto> getAllVehicles() {
        List<VehicleEntity> vehicleEntities = vehicleRepository.findAll();
        List<VehicleDto> vehicleDtoList = new ArrayList<>(vehicleEntities.size());
        for (VehicleEntity vehicle : vehicleEntities) {
            VehicleDto vehicleDto = mapper.mapToDto(vehicle);
            vehicleDto.setDriverId(vehicle.getDriver().getId());
            vehicleDtoList.add(vehicleDto);
        }
        return vehicleDtoList;
    }

    @Override
    public VehicleDto updateVehicle(int id, VehicleDto dto) {
        VehicleEntity vehicleEntity = vehicleRepository.findById(id).orElseThrow(() ->
                new NotFoundException("Vehicle with ID = " + id + " not found"));
        if (!isBlank(dto.getName())) {
            vehicleEntity.setName(dto.getName());
        }
        if (dto.getVehicleType() != null) {
            DriverEntity driver = vehicleEntity.getDriver();
            if (driver.getLicense().contains(dto.getVehicleType())) {
                vehicleEntity.setVehicleType(dto.getVehicleType());
            } else
                throw new BadRequestException("You can't change vehicle type because his driver have no license for it");
        }
        if (dto.getDriverId() != 0) {
            DriverEntity driver = driverRepository.findById(dto.getDriverId()).orElseThrow(() ->
                    new NotFoundException("Driver with ID = " + dto.getDriverId() + " not found"));
            if (driver.getLicense().contains(vehicleEntity.getVehicleType())
                    && driver.getVehicles().size() <= 2
                    && driver.getValidUntil().isAfter(LocalDate.now())) {

                vehicleEntity.setDriver(driver);
            } else throw new BadRequestException("You can't add this vehicle to driver with ID = " + driver.getId());
        }
        VehicleEntity updatedVehicle = vehicleRepository.save(vehicleEntity);
        VehicleDto returnDto = mapper.mapToDto(updatedVehicle);
        returnDto.setDriverId(vehicleEntity.getDriver().getId());
        return returnDto;
    }

    @Override
    public VehicleDto getVehicle(int id) {
        VehicleEntity vehicleEntity = vehicleRepository.findById(id).orElseThrow(() ->
                new NotFoundException("Vehicle with ID = " + id + " not found"));
        VehicleDto returnDto = mapper.mapToDto(vehicleEntity);
        returnDto.setDriverId(vehicleEntity.getDriver().getId());
        return returnDto;
    }

    @Override
    public void destroyVehicle(int id) {
        VehicleEntity vehicleEntity = vehicleRepository.findById(id).orElseThrow(() ->
                new NotFoundException("Vehicle with ID = " + id + " not found"));
        vehicleRepository.delete(vehicleEntity);
    }

    private boolean checkDriver(DriverEntity driver, VehicleDto dto) {
        if (driver.getVehicles().size() <= 2 &&
                driver.getValidUntil().isAfter(LocalDate.now()) &&
                driver.getLicense().contains(dto.getVehicleType())) {
            return true;
        } else throw new BadRequestException("You can't add this vehicle to driver with ID = " + dto.getDriverId());
    }

}
