package ru.solomennikov.LogistikAPI.mapper;

import org.mapstruct.Mapper;
import ru.solomennikov.LogistikAPI.dto.VehicleDto;
import ru.solomennikov.LogistikAPI.persist.entity.VehicleEntity;


@Mapper
public abstract class VehicleMapper implements AbstractMapper<VehicleDto, VehicleEntity>{
}
