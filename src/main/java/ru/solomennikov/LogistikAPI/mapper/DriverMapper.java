package ru.solomennikov.LogistikAPI.mapper;

import org.mapstruct.Mapper;
import ru.solomennikov.LogistikAPI.dto.DriverDto;
import ru.solomennikov.LogistikAPI.persist.entity.DriverEntity;


@Mapper
public abstract class DriverMapper implements AbstractMapper<DriverDto, DriverEntity> {
}
