package ru.solomennikov.LogistikAPI.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.solomennikov.LogistikAPI.dto.DriverDto;
import ru.solomennikov.LogistikAPI.persist.entity.DriverEntity;
import ru.solomennikov.LogistikAPI.persist.repository.DriverRepository;
import ru.solomennikov.LogistikAPI.service.DriverService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class DriversController {
    private final DriverService service;
    private final DriverRepository repository;

    @PostMapping("/drivers")
    public DriverDto addNewDriver(@RequestBody DriverDto dto) {
        DriverDto driverDto = service.addNewDriver(dto);
        return driverDto;
    }

    @GetMapping("/drivers/{id}")
    public DriverDto showDriver(@PathVariable("id") int id) {
        DriverDto driverDto = service.showDriverById(id);
        return driverDto;
    }

    @PutMapping("/drivers/{id}")
    public DriverDto updateDriver(@PathVariable("id") int id, @RequestBody DriverDto dto) {
        DriverDto driverDto = service.updateDriver(id, dto);
        return driverDto;
    }

    @GetMapping("/drivers")
    public List<DriverEntity> getAllDrivers() {
        List<DriverEntity> drivers = repository.findAllDrivers();
        return drivers;
    }

    @DeleteMapping("/drivers/{id}")
    public String destroyDriver(@PathVariable("id") int id) {
        service.destroy(id);
        return "Driver with ID = " + id + " has been deleted!";
    }
}
