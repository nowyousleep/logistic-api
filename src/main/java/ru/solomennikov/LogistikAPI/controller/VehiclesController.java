package ru.solomennikov.LogistikAPI.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.solomennikov.LogistikAPI.dto.VehicleDto;
import ru.solomennikov.LogistikAPI.service.VehicleService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class VehiclesController {
    private final VehicleService vehicleService;

    @PostMapping("/vehicles")
    public VehicleDto addNewVehicle(@RequestBody VehicleDto dto) {
        VehicleDto vehicleDto = vehicleService.addNewVehicle(dto);
        return vehicleDto;
    }

    @GetMapping("/vehicles")
    public List<VehicleDto> showAllVehicles() {
        List<VehicleDto> vehicleDtoList = vehicleService.getAllVehicles();
        return vehicleDtoList;
    }

    @GetMapping("/vehicles/{id}")
    public VehicleDto getVehicleById(@PathVariable("id") int id) {
        VehicleDto vehicleDto = vehicleService.getVehicle(id);
        return vehicleDto;
    }

    @PutMapping("/vehicles/{id}")
    public VehicleDto updateVehicle(@PathVariable("id") int id, @RequestBody VehicleDto dto) {
        VehicleDto updatedVehicle = vehicleService.updateVehicle(id, dto);
        return updatedVehicle;
    }

    @DeleteMapping("/vehicles/{id}")
    public String deleteVehicle(@PathVariable("id") int id) {
        vehicleService.destroyVehicle(id);
        return "Vehicle with ID = " + id + " has been deleted!";
    }

}
