package ru.solomennikov.LogistikAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogistikApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogistikApiApplication.class, args);
    }
}
