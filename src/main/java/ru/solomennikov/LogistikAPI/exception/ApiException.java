package ru.solomennikov.LogistikAPI.exception;

import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ApiException{
    private String info;
    private int status;
}