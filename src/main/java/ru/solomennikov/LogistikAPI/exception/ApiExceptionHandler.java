package ru.solomennikov.LogistikAPI.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler
    public ResponseEntity<ApiException> driverNotFound(NotFoundException ex) {
        ApiException exception = new ApiException();
        exception.setInfo(ex.getMessage());
        exception.setStatus(404);
        return new ResponseEntity<>(exception, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<ApiException> badRequest(BadRequestException ex) {
        ApiException exception = new ApiException();
        exception.setInfo(ex.getMessage());
        exception.setStatus(400);
        return new ResponseEntity<>(exception, HttpStatus.BAD_REQUEST);
    }
}
